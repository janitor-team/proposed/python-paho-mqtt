Source: python-paho-mqtt
Maintainer: Sebastian Reichel <sre@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pylama,
               python3-all,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.5.1
Homepage: https://www.eclipse.org/paho/clients/python/
Vcs-Browser: https://salsa.debian.org/debian/python-paho-mqtt
Vcs-Git: https://salsa.debian.org/debian/python-paho-mqtt.git

Package: python3-paho-mqtt
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: MQTT client class (Python 3)
 This code provides a client class which enable applications to connect
 to an MQTT broker to publish messages, and to subscribe to topics and
 receive published messages. It also provides some helper functions to
 make publishing one off messages to an MQTT server very straightforward.
 .
 The MQTT protocol is a machine-to-machine (M2M)/”Internet of Things”
 connectivity protocol. Designed as an extremely lightweight publish/
 subscribe messaging transport, it is useful for connections with remote
 locations where a small code footprint is required and/or network
 bandwidth is at a premium.
 .
 This is the Python 3 version of the package.
